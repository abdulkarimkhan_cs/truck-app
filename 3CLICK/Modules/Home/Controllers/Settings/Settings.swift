//
//  Settings.swift
//  3CLICK
//
//  Created by Sierra-PC on 23/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class Settings: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrSettingsOptions:[SettingsData] = [
        SettingsData(image: UIImage(named: "profile"), option: Strings.UPDATE_PROFILE.text),
        SettingsData(image: UIImage(named: "changePassword"), option: Strings.CHANGE_PASSWORD.text),
        SettingsData(image: UIImage(named: "bankingDetails"), option: Strings.ADD_BANKING_DETAILS.text)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.SETTINGS.text
        self.registerCell()
        // Do any additional setup after loading the view.
    }
}
extension Settings{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "SettingsTVC", bundle: nil), forCellReuseIdentifier: "SettingsTVC")
    }
}
extension Settings:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSettingsOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTVC", for: indexPath) as! SettingsTVC
        cell.setData(data: self.arrSettingsOptions[indexPath.row])
        return cell
    }
}
//MARK:- UITableViewDelegate
extension Settings: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case 0:
            super.pushToProfile()
        case 1:
            super.pushToChangePassword()
        default:
            super.pushToBankingDetails()
        }
    }
}

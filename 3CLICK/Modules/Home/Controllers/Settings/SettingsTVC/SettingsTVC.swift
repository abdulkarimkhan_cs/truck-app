//
//  SettingsTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 23/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

struct SettingsData {
    var image:UIImage?
    var option:String
}

class SettingsTVC: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblOption: UILabel!
    
    func setData(data:SettingsData){
        self.imgIcon.image = data.image
        self.lblOption.text = data.option
    }
}

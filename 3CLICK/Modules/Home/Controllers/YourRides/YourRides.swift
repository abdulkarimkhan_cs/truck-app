//
//  Home.swift
//  3CLICK
//
//  Created by Sierra-PC on 17/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class YourRides: BaseController {
    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.BOOKINGS.text
        self.setPageMenu()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callCurrentTabRides()
    }
}
//MARK:- Helper Methods
extension YourRides{
    private func setPageMenu(){
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
            

            let currentBookings = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "Rides") as! Rides
            currentBookings.bookingType = .current
            currentBookings.title = Strings.CURRENT_BOOKINGS.text
            currentBookings.selectedRide = { ride in
                self.getRideDetail(ride: ride.ride, rideType: .current)
            }
            controllerArray.append(currentBookings)
            
            let previousBookings = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "Rides") as! Rides
            previousBookings.bookingType = .finished
            previousBookings.title = Strings.PREVIOUS_BOOKINGS.text
            previousBookings.selectedRide = { ride in
                self.getRideDetail(ride: ride.ride, rideType: .finished)
            }
            controllerArray.append(previousBookings)
            
            // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
            // Example:
            let parameters: [CAPSPageMenuOption] = [
                .menuItemSeparatorWidth(0),
                .useMenuLikeSegmentedControl(true),
                .menuItemSeparatorPercentageHeight(0.1),
                .scrollMenuBackgroundColor(Global.APP_COLOR_GREY),
                .selectionIndicatorColor(Global.APP_COLOR),
                .selectedMenuItemLabelColor(Global.APP_COLOR),
                .unselectedMenuItemLabelColor(.black),
                .menuHeight(50.0)
            ]
            
            let bottomPadding = Constants.UIWINDOW?.safeAreaInsets.bottom ?? 0.0
            // Initialize page menu with controller array, frame, and optional parameters
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height - bottomPadding), pageMenuOptions: parameters)
            
            // Lastly add page menu as subview of base view controller view
            // or use pageMenu controller in you view hierachy as desired
            self.pageMenu?.delegate = self
            self.view.addSubview(pageMenu!.view)
        }
        private func callCurrentTabRides(){
            let currentIndex = self.pageMenu?.currentPageIndex ?? 0
            if let controller = self.pageMenu?.controllerArray[currentIndex] as? Rides{
                controller.refreshRides()
            }
        }
}
//MARK:- CAPSPageMenuDelegate
extension YourRides: CAPSPageMenuDelegate{
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        if let controller = controller as? Rides{
                controller.refreshRides()
        }
    }
}
//MARK:- Service
extension YourRides{
    private func getRideDetail(ride:Bookings,rideType:BookingType){
          let id = ride.id ?? ""
              APIManager.sharedInstance.usersAPIManager.RideDetail(params: [:], success: { (responseObject) in
                  let response = responseObject as Dictionary
                  response.printJson()
                  let rideDetail = Mapper<RideDetailModel>().map(JSON: responseObject) ?? RideDetailModel()
                  print(rideDetail)
                if rideDetail.bookingStatus == Strings.INPROGRESS.rawValue{
                    super.pushToHome(rideStatus: .endRide, rideDetail: rideDetail)
                }
                else{
                    super.pushToRideDetails(rideDetail: rideDetail, rideType: rideType, flagShouldChangeToRootView: false)
                }
              }, failure: { (error) in
                  print(error)
              }, id: id)
    }
}
//MARK:- Helper Methods
extension YourRides{
    private func checkRideStatus(ride:RideListModel,rideDetail:RideDetailModel,rideType:BookingType){
//        switch rideType {
//        case .finished:
//            super.pushToRideDetails(ride: ride, rideDetail: rideDetail, rideType: rideType,flagShouldChangeToRootView: false)
//        case .current:
//            let rideStatus = rideDetail.rideStatusId
//            switch rideStatus{
//            case CurrentRideStatus.approved.rawValue:
//                super.pushToRideDetails(ride: ride, rideDetail: rideDetail, rideType: rideType,flagShouldChangeToRootView: false)
////            case CurrentRideStatus.arrived.rawValue:
////                super.pushToHome(rideStatus: RideStatus.arrived, rideDetail: rideDetail)
//            case CurrentRideStatus.inProgress.rawValue:
//                super.pushToHome(rideStatus: RideStatus.endRide, rideDetail: rideDetail)
//            default:
//                break
//            }
//        }
    }
}

//<temp>

//
//  CustomerRating.swift
//  3CLICK
//
//  Created by Sierra-PC on 11/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class CustomerRating: BaseController {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.CUSTOMER_RATING.text
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnSubmit(_ sender: OrangeGradient) {
        AppDelegate.shared.changeRootViewController()
    }
    
}

//
//  Home.swift
//  3CLICK
//
//  Created by Sierra-PC on 27/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MBCircularProgressBar
import SwiftyJSON
import Alamofire
import SocketIO
import ObjectMapper

class Home: BaseController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewRideEnded: UIView!
    @IBOutlet weak var viewAcceptedRide: UIView!
    @IBOutlet weak var lblPickupTitle: UILabel!
    @IBOutlet weak var lblDropOffTitle: UILabel!
    @IBOutlet weak var lblEstimatedAmountTitle: UILabel!
    @IBOutlet weak var lblPickupLocation: UILabel!
    @IBOutlet weak var lblDropOffLocation: UILabel!
    @IBOutlet weak var lblEstimatedAmount: UILabel!
    @IBOutlet weak var btnArrived: OrangeGradient!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewRideInfo: UIStackView!
    @IBOutlet weak var viewRideInfoBG: OrangeGradient!
    
    let locationManager = CLLocationManager()
    var myCurrentLocation: CLLocation?
    var rideStatus = RideStatus.acceptedRide
    var rideDetail = RideDetailModel()
    var pickUpLocation : CLLocationCoordinate2D?
    var dropOffLocation: CLLocationCoordinate2D?
    var pickUpLocationMarker = GMSMarker()
    var dropOffLocationMarker = GMSMarker()
    var driverLocationMarker = GMSMarker()
    var isDriverPlaced = false
    var canFitToBounds = true
    var timerUpdateDriver: Timer!
    var timerDispatcherSourceTimer: DispatchSourceTimer?
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var pathIterator: UInt = 0
    var timer: Timer!
    var distanceCovered = "0"
    var flagIsRideEnded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.HOME.text
        self.setGMSMapView()
        self.setProgressBarUI()
        self.setUI()
        self.startTimer()
        self.mapView.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.setLocationManager()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setDropOffMarker()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disconnectSocket()
        self.stopTimer()
    }
    
    @IBAction func onBtnAccept(_ sender: OrangeGradient) {
        self.rideStatus = .acceptedRide
        self.setUI()
    }
    @IBAction func onBtnDone(_ sender: OrangeGradient) {
        AppDelegate.shared.changeRootViewController()
    }
    @IBAction func onBtnArrived(_ sender: OrangeGradient) {
        if self.rideStatus == .acceptedRide{
            Utility.main.showAlert(message: Strings.ARRIVED_AT_PICKUP.text, title: Strings.ALERT.text, controller: self) { (yes, no) in
                if yes != nil{
                    self.arrivedRide()
                    return
                }
            }
        }
        if self.rideStatus == .arrived{
            Utility.main.showAlert(message: Strings.ASK_START_RIDE.text, title: Strings.ALERT.text, controller: self) { (yes, no) in
                if yes != nil{
                    self.startRide()
                    return
                }
            }
        }
        if self.rideStatus == .endRide{
            Utility.main.showAlert(message: Strings.ASK_END_RIDE.text, title: Strings.ALERT.text, controller: self) { (yes, no) in
                if yes != nil{
                    self.emitEndRide()
                    return
                }
            }
        }
    }
}
//MARK:- Helper Methods
extension Home{
    private func setGMSMapView(){
        //self.mapView.isMyLocationEnabled = true
        //self.mapView.settings.myLocationButton = true
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 130, right: 6)
    }
    private func setLocationManager(){
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    private func zoomToSearchLocation(location: CLLocationCoordinate2D){
        CATransaction.begin()
        CATransaction.setValue(1.25, forKey: kCATransactionAnimationDuration)
        let fancy = GMSCameraPosition.camera(withLatitude: location.latitude,
                                             longitude: location.longitude,
                                             zoom: 18,
                                             bearing: 0,
                                             viewingAngle: 0)
        self.mapView.animate(to: fancy)
        CATransaction.commit()
    }
    private func setProgressBarUI(){
    }
    private func setUI(){
        self.title = Strings.YOUR_RIDE.text
        switch self.rideStatus{
        case .noRide:
           // self.viewRideEnded.isHidden = true
            self.viewAcceptedRide.isHidden = true
        case .acceptedRide:
            self.rideStatus = .arrived
            self.setUI()
        case .arrived:
           // self.viewRideEnded.isHidden = true
            self.viewAcceptedRide.isHidden = false
//            self.viewRideInfo.isHidden = true
//            self.viewRideInfoBG.isHidden = true
            self.btnArrived.setTitle("START RIDE", for: .normal)
        case .endRide:
            //self.viewRideEnded.isHidden = true
            self.viewAcceptedRide.isHidden = false
//            self.viewRideInfo.isHidden = true
//            self.viewRideInfoBG.isHidden = true
//            self.lblPickupTitle.text = "Total Time"
//            self.lblDropOffTitle.text = "Total Distance"
//            self.lblEstimatedAmountTitle.text = "Total Amount"
//            self.lblPickupLocation.text = "30 Minutes"
//            self.lblDropOffLocation.text = "10km"
//            self.lblEstimatedAmount.text = "\(Constants.Currency) 500"
            self.btnArrived.setTitle("END RIDE", for: .normal)
            self.connectSocket()
        case .rideEnded:
//            self.viewRideEnded.isHidden = false
            self.viewAcceptedRide.isHidden = true
//            self.viewRideInfo.isHidden = true
//            self.viewRideInfoBG.isHidden = true
        }
    }
    private func setDropOffMarker(){
        guard let dropOffLat = CLLocationDegrees(self.rideDetail.dropOffLatitude ?? "0.0") else {return}
        guard let dropOffLng = CLLocationDegrees(self.rideDetail.dropOffLongitude ?? "0.0") else {return}
        self.dropOffLocation = CLLocationCoordinate2D(latitude: dropOffLat, longitude: dropOffLng)
        self.addDropOffMarker()
        self.zoomToSearchLocation(location: self.dropOffLocation ?? CLLocationCoordinate2D())
    }
    private func fitAllMarkersBounds() {
        var bounds = GMSCoordinateBounds()
        var markerList = [GMSMarker]()
        if let pathBounds = self.getPathBounds(){
            markerList = pathBounds
        }
        markerList.append(self.pickUpLocationMarker)
        markerList.append(self.dropOffLocationMarker)
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(50))
        self.mapView.animate(with: update)
    }
    private func getPathBounds()->[GMSMarker]?{
        if self.path.count() == 0{return nil}
        var arrPinPoints = [GMSMarker]()
        let pathTaken = self.path.count()
        
        if pathTaken > 0{
            for i in 0..<pathTaken{
                let point = path.coordinate(at: i)
                let position = CLLocationCoordinate2D(latitude: point.latitude , longitude: point.longitude)
                arrPinPoints.append(GMSMarker(position: position))
            }
        }
        return arrPinPoints
    }
    private func getDistance()->String?{
        if self.path.count() == 0{return nil}
        var distance = 0.0
        let pathTaken = self.path.count()
        let p1 = path.coordinate(at: 0)
        var point1 = CLLocation(latitude: p1.latitude , longitude: p1.longitude)
        if pathTaken > 0{
            for i in 1..<pathTaken{
                let p2 = path.coordinate(at: i)
                let point2 = CLLocation(latitude: p2.latitude , longitude: p2.longitude)
                let dist = Double(point1.distance(from: point2))
                distance = distance + dist
                point1 = point2
            }
        }
        let distanceInKM = "\(distance/1000.0)"
        print(distanceInKM)
        return distanceInKM
    }
}
//MARK:- Handle Driver Marker
extension Home{
    private func handleDriverMarker(){
        if self.myCurrentLocation == nil{return}
        self.addDriverMarker()
        if self.canFitToBounds{
            self.fitAllMarkersBounds()
            self.canFitToBounds = false
        }
        //self.getRoute()
    }
    func startTimer() {
        if #available(iOS 10.0, *) {
            self.timerUpdateDriver = Timer.scheduledTimer(withTimeInterval: TimeInterval(5.0), repeats: true) { [weak self] _ in
                // do something here
                self?.handleDriverMarker()
            }
        }
    }
    func stopTimer() {
        self.timerUpdateDriver?.invalidate()
        //timerDispatchSourceTimer?.suspend() // if you want to suspend timer
        self.timerDispatcherSourceTimer?.cancel()
    }
}
//MARK:- Add marker
extension Home{
    private func addPickUpMarker(){
        var markerIcon: UIImageView?
        let pickUpPin = UIImage(named: "location_p")!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: pickUpPin)
        markerIcon = markerView
        markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
        self.pickUpLocationMarker.map = nil
        self.pickUpLocationMarker = GMSMarker(position: self.pickUpLocation ?? CLLocationCoordinate2D())
        self.pickUpLocationMarker.iconView = markerIcon
        self.pickUpLocationMarker.tracksViewChanges = true
        self.pickUpLocationMarker.map = self.mapView
    }
    private func addDropOffMarker(){
        var markerIcon: UIImageView?
        let dropOffPin = UIImage(named: "location_d")!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: dropOffPin)
        markerIcon = markerView
        markerIcon?.frame = CGRect(x: 0, y: 0, width: 40, height: 50)
        self.dropOffLocationMarker.map = nil
        self.dropOffLocationMarker = GMSMarker(position: self.dropOffLocation ?? CLLocationCoordinate2D())
        self.dropOffLocationMarker.iconView = markerIcon
        self.dropOffLocationMarker.tracksViewChanges = true
        self.dropOffLocationMarker.map = self.mapView
    }
    private func addDriverMarker(){
        if !self.isDriverPlaced{
            var markerIcon: UIImageView?
            let pickUpPin = UIImage(named: "location1")!.withRenderingMode(.alwaysOriginal)
            let markerView = UIImageView(image: pickUpPin)
            markerView.contentMode = .scaleAspectFit
            markerIcon = markerView
            markerIcon?.frame = CGRect(x: 0, y: 0, width: 50, height: 60)
            //self.driverLocationMarker.map = nil
            self.driverLocationMarker = GMSMarker(position: self.myCurrentLocation?.coordinate ?? CLLocationCoordinate2D())
            self.driverLocationMarker.iconView = markerIcon
            self.driverLocationMarker.tracksViewChanges = false
            self.driverLocationMarker.map = self.mapView
            self.isDriverPlaced = true
        }
        else{
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.driverLocationMarker.position =  self.myCurrentLocation?.coordinate ?? CLLocationCoordinate2D()
            CATransaction.commit()
        }
    }
}
//MARK:- CLLocationManagerDelegate
extension Home: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            break
        case .denied:
            Utility.main.showToast(message: "You denied the permission, your location tracking won't work")
            break
        default:
            print("Unknown Status")
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentlocation = locations.first else {return}
        self.myCurrentLocation = currentlocation
        self.addDriverMarker()
        if self.rideStatus == .endRide{
            self.emitLatLng()
        }
        //self.zoomToSearchLocation(location: currentlocation.coordinate)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Utility.main.showToast(message: "Did location updates is called but failed getting location \(error)")
    }
}
//MARK:- GMSMapViewDelegate
extension Home: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 15, execute: {
//            self.canFitToBounds = true
//        })
    }
}
//MARK:- Polyline
extension Home{
    private func getRoute(){
        let origin = "\(self.myCurrentLocation?.coordinate.latitude ?? 0.0),\(self.myCurrentLocation?.coordinate.longitude ?? 0.0)"
        let destination = "\(self.rideDetail.dropOffLatitude ?? "0.0"),\(self.rideDetail.dropOffLongitude ?? "0.0")"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.apiKey)"
        Alamofire.request(url).responseJSON { response in
            do {
                let json = try JSON(data: response.data ?? Data())
                let routes = json["routes"].arrayValue
                self.drawRoute(routesArray: routes)
            } catch {
                print("Hm, something is wrong here. Try connecting to the wifi.")
            }
        }
    }
    private func drawRoute(routesArray: [JSON]) {
        self.resetPolyline()
        if !routesArray.isEmpty{
            let routeDict = routesArray[0]
            let routeOverviewPolyline = routeDict["overview_polyline"].dictionary
            let points = routeOverviewPolyline?["points"]?.stringValue
            self.path = GMSPath.init(fromEncodedPath: points ?? "")!
            self.polyline.path = path
            self.polyline.strokeColor = Global.APP_COLOR
            self.polyline.strokeWidth = 3.0
            self.polyline.map = self.mapView
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
        }
    }
    @objc private func animatePolylinePath() {
//        if (self.pathIterator < self.path.count()) {
//            self.animationPath.add(self.path.coordinate(at: self.pathIterator))
//            self.animationPolyline.path = self.animationPath
//            self.animationPolyline.strokeColor = UIColor.lightGray
//            self.animationPolyline.strokeWidth = 3
//            self.animationPolyline.map = self.mapView
//            self.pathIterator += 1
//        }
//        else {
//            self.resetPolyline()
//        }
    }
    private func resetPolyline(){
        self.pathIterator = 0
        self.animationPath = GMSMutablePath()
        self.animationPolyline.map = nil
    }
}
//MARK:- Services
extension Home{
    private func arrivedRide(){
        self.rideStatus = .arrived
        self.setUI()
//        let rider_id = self.rideDetail.rideId
//        let device_token = Constants.DeviceToken
//        let params:[String:Any] = ["ride_id":rider_id,
//                                   "device_token":device_token]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.ArrivedRide(params: params, success: { (responseObject) in
//            print(responseObject)
//            self.rideStatus = .arrived
//            self.setUI()
//        }) { (error) in
//            print(error)
//        }
    }
    private func startRide(){
        self.rideStatus = .endRide
        self.setUI()
        //self.connectSocket()
//        let ride_id = self.rideDetail.rideId
//        let kilo_meter = self.getDistance() ?? "0"
//        let params:[String:Any] = ["ride_id":ride_id,"kilo_meter":kilo_meter]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.StartRide(params: params, success: { (responseObject) in
//            print(responseObject)
//            self.rideStatus = .endRide
//            self.setUI()
//            self.connectSocket()
//        }) { (error) in
//            print(error)
//        }
        let id = rideDetail.id ?? ""
        APIManager.sharedInstance.usersAPIManager.StartRide(params: [:], success: { (responseObject) in
            let response = responseObject as Dictionary
            response.printJson()
            self.rideStatus = .endRide
            self.setUI()
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
    private func endRide(){
//        let ride_id = self.rideDetail.rideId
//        let weight = self.rideDetail.weightInTons ?? "0"
//        let km = self.distanceCovered
//        let user_sub_type = AppStateManager.sharedInstance.loggedInUser.userSubType
//        let params:[String:Any] = ["ride_id":ride_id,
//                                   "weight":weight,
//                                   "km":km,
//                                   "user_sub_type":user_sub_type]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.EndRide(params: params, success: { (responseObject) in
//            print(responseObject)
//            self.rideStatus = .rideEnded
//            self.getRideDetail()
//        }) { (error) in
//            print(error)
//        }
        let id = rideDetail.id ?? ""
        APIManager.sharedInstance.usersAPIManager.EndRide(params: [:], success: { (responseObject) in
            let response = responseObject as Dictionary
            response.printJson()
            self.rideStatus = .rideEnded
            self.getRideDetail()
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
    private func getRideDetail(){
//        super.pushToRideDetails(rideDetail: rideDetail, rideType: .finished)
        super.pushToRideDetails(rideDetail: rideDetail, rideType: .finished, flagShouldChangeToRootView: true)
//        let ride_id = self.rideDetail.rideId
//        let param:[String:Any] = ["ride_id":ride_id]
//        print(param)
//        APIManager.sharedInstance.usersAPIManager.RideDetails(params: param, success: { (responseObject) in
//            print(responseObject)
//            let rideDetail = Mapper<RideDetailsModel>().map(JSON: responseObject) ?? RideDetailsModel()
//            super.pushToRideDetails(ride: RideListModel(), rideDetail: rideDetail, rideType: .previous,flagShouldChangeToRootView: true)
//        }) { (error) in
//            print(error)
//        }
    }
}
//MARK:- Socket
extension Home{
    open class SocketConnection {
        public static let default_ = SocketConnection()
        let manager: SocketManager
        private init() {
            //let token = AppStateManager.sharedInstance.loggedInUser.accessToken
            let param:[String:Any] = [:]//["token":token ?? ""]
            let route = "https://mto-socket.herokuapp.com/"//"http://192.168.10.232:3471/"
            let socketURL: URL = Utility.URLforRoute(route: route, params: param)! as URL
            manager = SocketManager(socketURL: socketURL, config: [.log(true), .compress])
            manager.config = SocketIOClientConfiguration(arrayLiteral: .connectParams(param), .secure(true))
        }
    }
    private func connectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        if socket.status != .connected{
            socket.connect()
        }
        socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print(ack)
            print("socket connected")
            self.getRideFinishAcknowledgement()
        }
        socket.on("unauthorized") { (data, ack) in
            print(data)
            print(ack)
            print("unauthorized user")
        }
    }
    private func disconnectSocket(){
        let socket = SocketConnection.default_.manager.defaultSocket
        socket.disconnect()
    }
    private func emitLatLng(){
        let socket = SocketConnection.default_.manager.defaultSocket
        if socket.status != .connected{return}
        let lat = "\(self.myCurrentLocation?.coordinate.latitude ?? 0.0)"
        let lng = "\(self.myCurrentLocation?.coordinate.longitude ?? 0.0)"
        let booking = self.rideDetail.id ?? "0"
        let params:[String:Any] = ["lat":lat,"lng":lng,"booking":booking] as Dictionary
        print(params)

        socket.emitWithAck("send-location-from-driver", params).timingOut(after: 5) {data in
            print(data)
        }
    }
    private func emitEndRide(){
        Utility.showLoader()
        let socket = SocketConnection.default_.manager.defaultSocket
        let booking = self.rideDetail.id ?? "0"
        let param:[String:Any] = ["booking":booking] as Dictionary
        socket.emitWithAck("ride-finished", param).timingOut(after: 5) {data in
            print(data)
            Utility.hideLoader()
        }
    }
    private func getRideFinishAcknowledgement(){
        let socket = SocketConnection.default_.manager.defaultSocket
        socket.on("ride-finished") {data, ack in
            print(data)
            guard let driverDistance = data.first as? NSDictionary else {return}
            guard let distance = driverDistance["distance"] as? String else {return}
            self.distanceCovered = distance
            socket.disconnect()
            if !self.flagIsRideEnded{
                self.endRide()
                self.flagIsRideEnded = true
            }
            
        }
    }
}

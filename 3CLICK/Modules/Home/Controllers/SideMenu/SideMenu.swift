//
//  SideMenu.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class SideMenu: UIViewController {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblRating: UILabel!
    
    var arrSideMenuData = [SideMenuData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.getRatings()
        // Do any additional setup after loading the view.
    }

}
//MARK:- Helper Methods
extension SideMenu{
    private func setData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        self.lblCustomerName.text = (user?.firstName ?? "") + " " + (user?.lastName ?? "")
        self.arrSideMenuData = [SideMenuData(icon: UIImage(named: "aboutUs"), option: Strings.ABOUT_US.text, notificationCount: nil),
                                           SideMenuData(icon: UIImage(named: "bell"), option: Strings.NOTIFICATIONS.text, notificationCount: 0),
                                            SideMenuData(icon: UIImage(named: "truckIcon"), option: Strings.TRANSACTIONS.text, notificationCount: nil),
                                            SideMenuData(icon: UIImage(named: "help"), option: Strings.HELP.text, notificationCount: nil),
                                            SideMenuData(icon: UIImage(named: "logout"), option:  Strings.LOGOUT.text, notificationCount: nil)]
               
      //  let userSubType = "\(user?.userSubType ?? 0)"
//        if userSubType != UserSubType.individualRider.rawValue{
//            self.ratingView.isHidden = true
//            self.arrSideMenuData = [SideMenuData(icon: UIImage(named: "aboutUs"), option: Strings.ABOUT_US.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "bell"), option: Strings.NOTIFICATIONS.text, notificationCount: 0),
//                                    SideMenuData(icon: UIImage(named: "truckIcon"), option: Strings.TRANSACTIONS.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "help"), option: Strings.HELP.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "logout"), option:  Strings.LOGOUT.text, notificationCount: nil)]
//        }
//        else{
//            self.arrSideMenuData = [SideMenuData(icon: UIImage(named: "aboutUs"), option: Strings.ABOUT_US.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "settings"), option: Strings.SETTINGS.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "bell"), option: Strings.NOTIFICATIONS.text, notificationCount: 0),
//                                    SideMenuData(icon: UIImage(named: "truckIcon"), option: Strings.TRANSACTIONS.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "earnings"), option: Strings.EARNINGS.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "help"), option: Strings.HELP.text, notificationCount: nil),
//                                    SideMenuData(icon: UIImage(named: "logout"), option:  Strings.LOGOUT.text, notificationCount: nil)]
//        }
//        if let imgURL = URL(string: user?.imageUrl ?? ""){
//            self.imgProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "profilePlaceholder"), options: .highPriority, completed: nil)
//        }
    }
}
extension SideMenu: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSideMenuData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC", for: indexPath) as! SideMenuTVC
        cell.setData(data: self.arrSideMenuData[indexPath.row])
        return cell
    }
}
extension SideMenu: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.changeContentWith(index: indexPath.row)
    }
}
extension SideMenu{
    private func changeContentWith(index:Int){
        if index == self.arrSideMenuData.count - 1{
            AppStateManager.sharedInstance.logoutUser()
            return
        }
        self.sideMenuController?.hideLeftViewAnimated()
        let user = AppStateManager.sharedInstance.loggedInUser
        switch index {
                    case 0:
                        self.changeContentToAboutUs()
                    case 1:
                        self.changeContentToNotifications()
                    case 2:
                        self.changeContentToYourRides()
                    case 3:
                        self.changeContentToHelp()
                    default:
                        break
                    }
//        let userSubType = "\(user?.userSubType ?? 0)"
//        if userSubType != UserSubType.individualRider.rawValue{
//            switch index {
//            case 0:
//                self.changeContentToAboutUs()
//            case 1:
//                self.changeContentToNotifications()
//            case 2:
//                self.changeContentToYourRides()
//            case 3:
//                self.changeContentToHelp()
//            default:
//                break
//            }
//        }
//        else{
//            switch index {
//            case 0:
//                self.changeContentToAboutUs()
//            case 1:
//                self.changeContentToSettings()
//            case 2:
//                self.changeContentToNotifications()
//            case 3:
//                self.changeContentToYourRides()
//            case 4:
//                self.changeContentToEarnings()
//            case 5:
//                self.changeContentToHelp()
//            default:
//                break
//            }
//        }
    }
    private func changeContentToYourRides(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "YourRides")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToEarnings(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Earnings")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Profile")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToNotifications(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Notifications")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToAboutUs(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .aboutUs
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToHelp(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .help
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
    private func changeContentToSettings(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Settings")
        let navController = BaseNavigationController(rootViewController: controller)
        self.sideMenuController?.rootViewController = navController
    }
}
extension SideMenu{
    private func getRatings(){
//        let userSubType = "\(AppStateManager.sharedInstance.loggedInUser.userSubType)"
//        if userSubType != UserSubType.individualRider.rawValue{return}
//        APIManager.sharedInstance.usersAPIManager.GetRatings(params: [:], success: { (responseObject) in
//            print(responseObject)
//            let response = responseObject as NSDictionary
//            if let ratings = response["rating"] as? Double{
//                self.ratingView.rating = ratings
//                self.lblRating.text = String(ratings)
//            }
//            else{
//                self.ratingView.rating = 0.0
//            }
//        }) { (error) in
//            print(error)
//        }
    }
}

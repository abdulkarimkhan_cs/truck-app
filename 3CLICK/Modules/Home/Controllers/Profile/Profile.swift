//
//  Profile.swift
//  3CLICK
//
//  Created by Sierra-PC on 12/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import MobileCoreServices
import PhoneNumberKit
import DropDown

class Profile: BaseController {

    @IBOutlet weak var imgProfile                : RoundedImage!
    @IBOutlet weak var imgProfileBackground      : UIImageView!
    @IBOutlet weak var tfEmail                   : UITextField!
    @IBOutlet weak var tfName                    : UITextField!
    @IBOutlet weak var tfState                   : UITextField!
    @IBOutlet weak var tfAge                     : UITextField!
    @IBOutlet weak var tfVehicleRegistration     : UITextField!
    @IBOutlet weak var switchGoodsInTransit      : UISwitch!
    @IBOutlet weak var tfCurrencyOfCertification : UITextField!
    @IBOutlet weak var tfDrivingLicense          : UITextField!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfCountryCode: UITextFieldDynamicFonts!
    @IBOutlet weak var tfPhoneNumber: UITextFieldDynamicFonts!
    
    var pickedImage: UIImage?
    var profile_image = ""
    
    let stateDropDown = DropDown()
    var arrStates = ["1","2","3","4"]
    var selectedState: String?
    
    var shouldVerfiyPin: Bool = false
    
    var minDate: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .year, value: -18, to: Date(), options: [])!
    }
    
    var certificate:NSData?
    var arrFiles = [UploadFileData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.PROFILE.text
        self.setData()
        self.getStates()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTfAgePicker(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    @IBAction func onBtnCamera(_ sender: RoundedButton) {
        self.chooseImageUploadType()
    }
    @IBAction func onBtnCountryCode(_ sender: UIButton) {
        self.openCountryPicker()
    }
    @IBAction func onBtnUpdate(_ sender: BlueGradient) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension Profile{
    private func setData(){
        let data = AppStateManager.sharedInstance.loggedInUser
   //     if let imageURL = URL(string: data?.imageUrl ?? ""){
   //         self.imgProfile.sd_setImage(with: imageURL, completed: nil)
     //       self.imgProfileBackground.sd_setImage(with: imageURL, completed: nil)
       // }
//        self.tfEmail.text                   = data?.email
//        self.tfName.text                    = data?.name
//        if let phoneNumber = data?.mobileNumber, !phoneNumber.isEmpty{
//            let phoneNumberKit = PhoneNumberKit()
//            do {
//                //let phoneNumber = "+923042009909"//"+12025550191"
//                let phoneNum = try phoneNumberKit.parse(phoneNumber)
//                let regionCode = phoneNumberKit.getRegionCode(of: phoneNum)
//                self.imgCountryFlag.image = Utility.emojiFlag(regionCode: regionCode ?? "PK")?.image()
//                let countryCode = try phoneNumberKit.parse(phoneNumber).countryCode
//                let code = "+\(countryCode)"
//                self.tfCountryCode.text = code
//                self.tfPhoneNumber.text = phoneNumber.replacingOccurrences(of: "\(code)", with: "")
//            }
//            catch {
//                print("Generic parser error")
//            }
//        }
//        else{
            //self.setPhoneNumberUI()
        //}
//        self.tfState.text = data?.stateName
//        let selectedState = ""
//        selectedState.stateId = "\(data?.stateId ?? 0)"
//        selectedState.stateName = data?.stateName ?? ""
//        self.selectedState = selectedState
    }
    private func validate(sender:UIButton){
        let number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        do {
            let _ = try self.tfEmail.validatedText(validationType: ValidatorType.email)
            let _ = try self.tfName.validatedText(validationType: ValidatorType.username)
            if !Utility.validatePhoneNumber(number: number){
                sender.shake()
                Utility.main.showToast(message: Strings.INVALID_PHONE.text)
                return
            }
            let _ = try self.tfState.validatedText(validationType: ValidatorType.state)
            self.verifyUpdatedData()
            self.uploadProfileImage()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
    private func setPhoneNumberUI(){
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: "AU")?.image()
        self.tfCountryCode.text = "+61"
    }
    private func openCountryPicker(){
        let controller = MICountryPicker()
        controller.delegate = self
        controller.showCallingCodes = true
 //       controller.setStatusBarStyle(.lightContent)
        self.present(controller, animated: true, completion: nil)
    }
    private func openCurrencyOfCertification(){
        let user = AppStateManager.sharedInstance.loggedInUser
//        if let fileURL = URL(string: user?.documentUrl ?? ""){
//            Utility.main.openURL(url: fileURL)
//        }
    }
    private func setStatesDropDown(){
        self.stateDropDown.dataSource = ["KHI","LHR","SKT","DAP"]
        self.stateDropDown.direction = .any
        self.stateDropDown.anchorView = self.tfState
        self.stateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfState.text = item
            self.selectedState = self.arrStates[index]
        }
    }
    private func verifyUpdatedData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        let email = user?.email ?? ""
        let mobileNumber = user?.contact ?? ""
        
        let newEmail = self.tfEmail.text ?? ""
        let newMobileNumber = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        
        if email != newEmail || mobileNumber != newMobileNumber{
            self.shouldVerfiyPin = true
        }
    }
}
//MARK:- Date Picker
extension Profile{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        let selectedYear = Int(dateFormatter.string(from: date)) ?? 0
        let currentYear = Int(dateFormatter.string(from: Date())) ?? 0
        let age = currentYear - selectedYear
        self.tfAge.text = "\(age)"
    }
}
//MARK:- MICountryPickerDelegate
extension Profile: MICountryPickerDelegate{
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        self.dismiss(animated: true, completion: nil)
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: code)?.image()
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.tfCountryCode.text = dialCode
    }
}
//MARK:- Services
extension Profile: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.tfCurrencyOfCertification == textField{
            self.openCurrencyOfCertification()
            return false
        }
        if self.tfState == textField{
            self.stateDropDown.show()
            return false
        }
        return true
    }
}
//MARK:- Image Picker
extension Profile{
    private func chooseImageUploadType(){
        let alert = UIAlertController(title: "Upload profile photo" , message: "How do you want to set your profile picture?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    private func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    private func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
    }
}
//MARK: - Image Picker
extension Profile: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.imgProfile.image = pickedImage
            self.imgProfileBackground.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Services
extension Profile{
    private func getStates(){
        self.setStatesDropDown()
//
//        APIManager.sharedInstance.usersAPIManager.GetStates(params: [:], success: { (responseObject) in
//            guard let response = responseObject as? [[String:Any]] else {return}
//            self.arrStates = Mapper<StateModel>().mapArray(JSONArray: response)
//            self.setStatesDropDown()
//        }) { (error) in
//            print(error)
//        }
    }
    private func uploadProfileImage(){
       self.updateUserProfile()

//        if let profileImage = self.pickedImage{
//            let file = profileImage.jpegData(compressionQuality: 0.01) ?? Data()
//            let param:[String:Any] = ["file":file]
//            APIManager.sharedInstance.usersAPIManager.UploadFile(params: param, success: { (responseObject) in
//                print(responseObject)
//                let response = responseObject as Dictionary
//                if let doc = response["doc"] as? String{
//                    self.profile_image = doc
//                }
//                self.updateUserProfile()
//            }) { (error) in
//                print(error)
//            }
//        }
//        else{
//            self.updateUserProfile()
//        }
    }
    private func updateUserProfile(){
        Utility.main.showAlert(message: Strings.PROFILE_UPDATED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
        //                guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
        //                let loggedInUser = AppStateManager.sharedInstance.loggedInUser
        //                user.token = loggedInUser?.token ?? ""
        //                AppStateManager.sharedInstance.loginUser(user: user)
        //
            if self.shouldVerfiyPin{
                super.pushToCodeVerification(user: UserModel())
            }
            else{
                Utility.main.showAlert(message: Strings.PROFILE_UPDATED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler:{
                    //  guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
                    AppStateManager.sharedInstance.loginUser(user: UserModel())
                })
                
            }
            
        })
//        let email = self.tfEmail.text ?? ""
//        let name = self.tfName.text ?? ""
//        let mobile_number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
//        let state_id = self.selectedState?.stateId ?? ""
//        let user_type = AppStateManager.sharedInstance.loggedInUser.userType
//        let user_sub_type = AppStateManager.sharedInstance.loggedInUser.userSubType
//        let device_token = Constants.DeviceToken
//        let network_protocol = "APNS"
//
//        let params:[String:Any] = ["profile_image":self.profile_image,
//                                   "email":email,
//                                   "name":name,
//                                   "mobile_number":mobile_number,
//                                   "state_id":state_id,
//                                   "user_type":user_type,
//                                   "user_sub_type":user_sub_type,
//                                   "device_token":device_token,
//                                   "network_protocol":network_protocol]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.UpdateUserProfile(params: params, success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message: Strings.PROFILE_UPDATED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
////                guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
////                let loggedInUser = AppStateManager.sharedInstance.loggedInUser
////                user.token = loggedInUser?.token ?? ""
////                AppStateManager.sharedInstance.loginUser(user: user)
////
//                if self.shouldVerfiyPin{
//                    super.pushToCodeVerification()
//                }
//                else{
//                    Utility.main.showAlert(message: Strings.PROFILE_UPDATED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler:{
//                        guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
//                        AppStateManager.sharedInstance.loginUser(user: user)
//                    })
//                }
//            })
//        }) { (error) in
//            print(error)
//        }
    }
}

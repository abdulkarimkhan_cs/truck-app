//
//  BankingDetails.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/28/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class BankingDetails: BaseController {

    @IBOutlet weak var tfAccountNumber: UITextFieldDynamicFonts!
    @IBOutlet weak var tfBSB: UITextFieldDynamicFonts!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.BANKING_DETAILS.text
        // Do any additional setup after loading the view.
    }
    

    @IBAction func onBtnSubmit(_ sender: OrangeGradient) {
        Utility.main.showToast(message: "Will be implemented in next sprint.")
    }
}

//
//  FleetDetails.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class FleetDetails: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.FLEET_DETAILS.text
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnHire(_ sender: OrangeGradient) {
        let title = Strings.HIRE_FLEET.text
        let message = Strings.ASK_TO_HIRE_FLEET.text
        Utility.main.showAlert(message: message, title: title, controller: self) { (yes, no) in
            if yes != nil{
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
}

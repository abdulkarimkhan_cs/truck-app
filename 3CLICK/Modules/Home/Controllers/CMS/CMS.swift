//
//  Help.swift
//  3CLICK
//
//  Created by Sierra-PC on 23/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class CMS: BaseController {
    
    var cms:CMSModel?
    var cmsType = CMSType.help
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        //self.webView.navigationDelegate = self
        //self.getContentIfRequiredOrLoadLink()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension CMS{
    private func setUI(){
        switch self.cmsType{
        case .help:
            self.title = Strings.HELP.text
        case .aboutUs:
            self.title = Strings.ABOUT_US.text
        case .termsAndConditions:
            break
        }
    }
}

//MARK:- Service
extension CMS{
    private func getCMS(){
        AppDelegate.shared.cms = cms
//        APIManager.sharedInstance.usersAPIManager.CMS(params: [:], success: { (responseObject) in
//            guard let cms = Mapper<CMSModel>().map(JSON: responseObject) else{return}
//            AppDelegate.shared.cms = cms
//            self.getContentIfRequiredOrLoadLink()
//        }) { (error) in
//            print(error)
//        }
    }
}

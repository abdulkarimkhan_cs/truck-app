//
//  EarningsTitleView.swift
//  3CLICK
//
//  Created by Sierra-PC on 12/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class EarningsTitleView: UIView {

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "EarningsTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}

//
//  Earnings.swift
//  3CLICK
//
//  Created by Sierra-PC on 12/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import Charts
import ObjectMapper

class Earnings: BaseController {
    
    @IBOutlet weak var lblTotalEarning: UILableDynamicFonts!
    @IBOutlet weak var lblRatings: UILableDynamicFonts!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblTotalRides: UILableDynamicFonts!
    @IBOutlet weak var lblCompletedRides: UILableDynamicFonts!
    @IBOutlet weak var lblCancelledRides: UILableDynamicFonts!
    @IBOutlet weak var lblRejectedRides: UILableDynamicFonts!
    
    var earnings: EarningModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.EARNINGS.text
        self.getEarnings()
        // Do any additional setup after loading the view.
    }
    
}
extension Earnings{
    private func setData(){
        self.lblTotalEarning.text = "\(Constants.Currency) \(self.earnings?.totalEarning ?? "")"
       self.lblRatings.text = self.earnings?.averageRating
        self.ratingView.rating = Double(self.earnings?.averageRating ?? "0") ?? 0.0
       self.lblTotalRides.text = self.earnings?.totalRides
       self.lblCompletedRides.text = self.earnings?.completedRides
        self.lblCancelledRides.text = self.earnings?.cancelledRides
        self.lblRejectedRides.text = self.earnings?.cancelledRides
        
    }
}
//MARK:- Services
extension Earnings{
    private func getEarnings(){
        self.setData()
//        APIManager.sharedInstance.usersAPIManager.GetEarnings(params: [:], success: { (responseObject) in
//            self.earnings = Mapper<EarningModel>().map(JSON: responseObject)
//            self.setData()
//        }) { (error) in
//            print(error)
//        }
    }
}

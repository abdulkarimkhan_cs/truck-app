//
//  EarningsTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 12/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class EarningsTVC: UITableViewCell {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    func setData(data:EarningModel,index:Int){
        if index%2 == 0{
            self.viewContent.backgroundColor = Global.APP_COLOR_GREY
        }
        else{
            self.viewContent.backgroundColor = .white
        }
    }

}

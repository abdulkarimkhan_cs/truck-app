//
//  EarningChartTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 12/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import Charts

class EarningChartTVC: UITableViewCell {

    @IBOutlet weak var chartView: BarChartView!
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setData()
    }
    
    func setData(){
        var dataEntries = [BarChartDataEntry]()
        let dataEntry = BarChartDataEntry(x: Double(10), y: Double(10))
        dataEntries.append(dataEntry)
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Visitor count")
        let chartData = BarChartData(dataSet: chartDataSet)
        if self.chartView != nil{
            chartView.data = chartData
        }
    }
}

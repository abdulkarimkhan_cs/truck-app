//
//  FleetsFilter.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

struct FleetsFilterData {
    var title:String
    var isSelected:Bool
}

class FleetsFilter: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrFleetsFilterData = [FleetsFilterData(title: "Fleet Company", isSelected: false),
                               FleetsFilterData(title: "Individual", isSelected: true),
                               FleetsFilterData(title: "Both", isSelected: false)]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func onBtnOk(_ sender: OrangeGradient) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- UITableViewDataSource
extension FleetsFilter:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFleetsFilterData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FleetsFilterTVC", for: indexPath) as! FleetsFilterTVC
        let data = self.arrFleetsFilterData[indexPath.row]
        cell.setData(data)
        return cell
    }
}
//MARK:- UITableViewDelegate
extension FleetsFilter:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrFleetsFilterData[indexPath.row].isSelected{return}
        for i in 0..<self.arrFleetsFilterData.count{
            self.arrFleetsFilterData[i].isSelected = false
        }
        self.arrFleetsFilterData[indexPath.row].isSelected = true
        self.tableView.reloadSections([0], with: .fade)
    }
}

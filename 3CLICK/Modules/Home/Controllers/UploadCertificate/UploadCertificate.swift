//
//  UploadCertificate.swift
//  LHRL
//
//  Created by Sierra-PC on 24/04/2019.
//  Copyright © 2019 Viftech Solution. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import DZNEmptyDataSet

enum UploadFileType{
    case photo
    case video
    case file
}

struct UploadFileData {
    var fileName: String
    var fileType: UploadFileType
    var fileData: NSData
}

class UploadCertificate: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddFile: UIBarButtonItem!
    
    var arrFiles = [UploadFileData]()
    var selectedFiles: (([UploadFileData])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnActionBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionAdd(_ sender: UIBarButtonItem) {
        //  clickFunction()
        if self.arrFiles.count == 1{
           Utility.main.showAlert(message: "You can not select more that 1 file.", title: "Alert")
        }else{
            self.chooseFileUploadType()
            //UploadFilesViewController.shared.showAttachmentActionSheet(vc: self)
        }
    }
    @IBAction func onBtnUploadFile(_ sender: UIButton) {
        self.selectedFiles?(self.arrFiles)
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- UI Related methods
extension UploadCertificate{
    @objc func onBtnDeleteFile(sender: UIButton){
        self.arrFiles.remove(at: sender.tag)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
//MARK:- UITableViewDataSource
extension UploadCertificate:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFiles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FileTableViewCell
        cell.setData(fileNo: indexPath.row + 1, data: self.arrFiles[indexPath.row])
        cell.fileDeleteIcon.tag = indexPath.row
        cell.fileDeleteIcon.addTarget(self, action: #selector(self.onBtnDeleteFile(sender:)), for: .touchUpInside)
        return cell
    }
}
//MARK:- UITableViewDelegate
extension UploadCertificate:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.downloadFiles(index: indexPath.row)
    }
}
//MARK:- Helper Methods
extension UploadCertificate{
    private func chooseFileUploadType(){
        let alert = UIAlertController(title: "Upload Document" , message: "How do you want to upload your document?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "iCloud Drive", style: .default, handler: { (UIAlertAction) in
            self.uploadFromiCloudDrive()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
//        alert.addAction(UIAlertAction(title: "Video", style: .default, handler: { (UIAlertAction) in
//            self.uploadVideo()
//        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    private func uploadFromiCloudDrive(){
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    private func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    private func uploadVideo(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .photoLibrary
            picker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            self.present(picker, animated: true, completion: nil)
        }
    }
    private func addDocumentIfNotExist(file : UploadFileData){
        if self.arrFiles.contains(where: {$0.fileName.elementsEqual(file.fileName)}) {
            DispatchQueue.main.async {
                switch file.fileType{
                case .photo:
                    Utility.main.showAlert(message: Strings.PHOTO_ALREADY_ADDED.text, title: Strings.Confirmation.text)
                case .video:
                    Utility.main.showAlert(message: Strings.VIDEO_ALREADY_ADDED.text, title: Strings.Confirmation.text)
                case .file:
                    Utility.main.showAlert(message: Strings.DOCUMENT_ALREADY_ADDED.text, title: Strings.Confirmation.text)
                }
            }
        } else {
            self.arrFiles.append(file)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                switch file.fileType{
                case .photo:
                    Utility.main.showAlert(message: Strings.PHOTO_ADDED_SUCCESSFULLY.text, title: Strings.Confirmation.text)
                case .video:
                    Utility.main.showAlert(message: Strings.VIDEO_ADDED_SUCCESSFULLY.text, title: Strings.Confirmation.text)
                case .file:
                    Utility.main.showAlert(message: Strings.DOCUMENT_ADDED_SUCCESSFULLY.text, title: Strings.Confirmation.text)
                }
            }
        }
    }
    private func setTableView(){
        self.tableView.emptyDataSetSource = self
        self.tableView.tableFooterView = UIView()
    }
}
//MARK: - Image & Video Picker
extension UploadCertificate: UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        var file: UploadFileData?
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            guard let pickedImageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL else {return}
            guard let imageData: NSData = NSData(data: pickedImage.jpegData(compressionQuality: 0.5)!) as NSData? else{return}
            file = UploadFileData(fileName: pickedImageURL.absoluteString, fileType: .photo, fileData: imageData)
        }else{
            let outputFileURL: URL = (info[UIImagePickerController.InfoKey.mediaURL] as! URL)
            guard let data = NSData(contentsOf: outputFileURL as URL) else {return}
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
            self.compressVideo(inputURL: outputFileURL as URL, outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    print("waiting")
                    break
                case .exporting:
                    print("exporting")
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {return}
                    print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                    print("completed")
                    file = UploadFileData(fileName: "\(outputFileURL)", fileType: .video, fileData: compressedData)
                case .failed:
                    break
                case .cancelled:
                    break
                }
            }
        }
        self.dismiss(animated: true, completion: {
            guard let mediaFile = file else {return}
            self.addDocumentIfNotExist(file: mediaFile)
        })
    }
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}
//MARK: - Document Picker
extension UploadCertificate: UIDocumentPickerDelegate,UIDocumentMenuDelegate{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileManager = FileManager.default
        print(fileManager.fileExists(atPath: url.path))
        let data = NSData(contentsOfFile: url.path)
        guard let doc = data else {
            Utility.main.showToast(message: "Document format not supported!")
            return
        }
        let file = UploadFileData(fileName: "\(url)", fileType: .file, fileData: doc)
        self.addDocumentIfNotExist(file: file)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("we cancelled")
        dismiss(animated: true, completion: nil)
    }
}
//MARK:- DZNEmptyDataSetSource
extension UploadCertificate:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_CERTIFICATES.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_CERTIFICATES_DESC.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}

//
//  FileTableViewCell.swift
//  LHRL
//
//  Created by Shakeel Khan on 1/24/19.
//  Copyright © 2019 Viftech Solution. All rights reserved.
//

import UIKit

class FileTableViewCell: UITableViewCell {

    @IBOutlet weak var fileIconImage: UIImageView!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var fileDeleteIcon: UIButton!
    
    func setData(fileNo:Int,data:UploadFileData){
        self.selectionStyle = .none
        switch data.fileType {
        case .photo:
            self.lblFileName.text = "Image file No. \(fileNo)"
            self.fileIconImage.image = UIImage(named:"fileImage")
        case .video:
            self.lblFileName.text = "Video file No. \(fileNo)"
            self.fileIconImage.image = UIImage(named:"fileVideo")
        case .file:
            self.lblFileName.text = "Document file No. \(fileNo)"
            self.fileIconImage.image = UIImage(named:"fileDoc")
        }
    }
//    func setData(fileNo:Int,data:GetCasesFilesDataModel){
//        self.selectionStyle = .none
//        self.fileDeleteIcon.isHidden = true
//        let fileType = self.getUploadFileType(fileName: data.Description)
//        switch fileType {
//        case .photo:
//            self.lblFileName.text = "Image file No. \(fileNo)"
//            self.fileIconImage.image = UIImage(named:"fileImage")
//        case .video:
//            self.lblFileName.text = "Video file No. \(fileNo)"
//            self.fileIconImage.image = UIImage(named:"fileVideo")
//        case .file:
//            self.lblFileName.text = "Document file No. \(fileNo)"
//            self.fileIconImage.image = UIImage(named:"fileDoc")
//        }
//    }
}
extension FileTableViewCell{
    private func getUploadFileType(fileName:String)->UploadFileType{
        let exten = fileName.components(separatedBy: ".").last ?? ""
        switch exten {
        case "jpeg":
            return UploadFileType.photo
        case "mp4":
            return UploadFileType.video
        default:
            return UploadFileType.file
        }
    }
}

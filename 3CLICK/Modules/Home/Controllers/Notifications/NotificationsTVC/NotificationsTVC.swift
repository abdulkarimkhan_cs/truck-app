//
//  NotificationsTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 22/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class NotificationsTVC: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    func setData(data:NotificationModel){
        self.lblTitle.text = data.title ?? "-"
        self.lblDescription.text = data.message ?? "-"
    }
    
}

//
//  Notifications.swift
//  3CLICK
//
//  Created by Sierra-PC on 22/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper
import DZNEmptyDataSet

class Notifications: BaseController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClearAll: UIButton!
    
    var arrNotifications = [NotificationModel]()
    var notificationsPaged = PagedModel()
    var pageNumber = 1
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.NOTIFICATIONS.text
        self.setUI()
        self.setTableView()
        self.getNotifications()
        self.addRefreshControl()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnClearAll(_ sender: UIButton) {
        Utility.main.showAlert(message: Strings.DELETE_NOTIFICATIONS.text, title: Strings.ALERT.text, controller: self) { (yes, no) in
            if yes != nil{
                self.removeAllNotifications()
            }
        }
    }
}
extension Notifications{
    private func setTableView(){
        self.tableView.emptyDataSetSource = self
        self.registerCell()
    }
    private func registerCell(){
        self.tableView.register(UINib(nibName: "NotificationsTVC", bundle: nil), forCellReuseIdentifier: "NotificationsTVC")
    }
    private func setUI(){
        self.btnClearAll.isHidden = false
        if self.arrNotifications.isEmpty{
            self.btnClearAll.isHidden = true
        }
        else{
            self.btnClearAll.isHidden = false
        }
    }
    private func loadMoreCells(){
        let totalNotifications = self.notificationsPaged.count
        if totalNotifications > self.arrNotifications.count && self.pageNumber <= self.notificationsPaged.totalPages{
            self.pageNumber += 1
            self.getNotifications()
        }
    }
    private func addRefreshControl(){
        self.refreshControl.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH.text)
        self.refreshControl.addTarget(self, action: #selector(self.refreshNotifications), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc func refreshNotifications() {
        // Code to refresh table view
        self.pageNumber = 0
        self.arrNotifications.removeAll()
        self.tableView.reloadData()
        self.getNotifications()
    }
}
extension Notifications:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotifications.count
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTVC", for: indexPath) as! NotificationsTVC
        let data = self.arrNotifications[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}
extension Notifications:UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if tableView.visibleCells.contains(cell) {
                if indexPath.row == self.arrNotifications.count - 1{
                    self.loadMoreCells()
                }
            }
        }
    }
}
extension Notifications:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_NOTIFICATIONS.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_NOTIFICATIONS_DESC.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
//MARK:- Services
extension Notifications{
    private func getNotifications(){
        let page = self.pageNumber
        let limit = Constants.PAGINATION_PAGE_SIZE
        
        let params:[String:Any] = ["page":page,
                                   "limit":limit]
        
        APIManager.sharedInstance.usersAPIManager.Notifications(params: params, success: { (responseObject) in
            print(responseObject)
            
            let response = responseObject as Dictionary
            response.printJson()
            guard let bookings = response["notifications"] as? [[String:Any]] else {return}
            
            if self.arrNotifications.isEmpty{
                self.notificationsPaged = Mapper<PagedModel>().map(JSON: responseObject) ?? PagedModel()
                self.arrNotifications = Mapper<NotificationModel>().mapArray(JSONArray: bookings)
            }
            else{
                let notifications = Mapper<NotificationModel>().mapArray(JSONArray: bookings)
                self.arrNotifications += notifications
            }
            self.setUI()
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
    private func removeAllNotifications(){
        APIManager.sharedInstance.usersAPIManager.RemoveAllNotifications(params: [:], success: { (responseObject) in
            self.arrNotifications.removeAll()
            self.setUI()
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
}

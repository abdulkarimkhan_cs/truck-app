//
//  ChangePassword.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class ChangePassword: BaseController {
    
    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.CHANGE_PASSWORD.text
    }
    
    @IBAction func onBtnShowPassword(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.tfOldPassword.isSecureTextEntry = !self.tfOldPassword.isSecureTextEntry
        case 1:
            self.tfNewPassword.isSecureTextEntry = !self.tfNewPassword.isSecureTextEntry
        case 2:
            self.tfConfirmPassword.isSecureTextEntry = !self.tfConfirmPassword.isSecureTextEntry
        default:
            break
        }
    }
    @IBAction func onBtnChangePassword(_ sender: OrangeGradient) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension ChangePassword{
    private func validate(sender:UIButton){
        let newPassword = self.tfNewPassword.text ?? ""
        let confirmPassword = self.tfConfirmPassword.text ?? ""
        do {
            let _ = try self.tfOldPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.OLD_PASSWORD.text))
            let _ = try self.tfOldPassword.validatedText(validationType: ValidatorType.password)
            let _ = try self.tfNewPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.NEW_PASSWORD.text))
            let _ = try self.tfNewPassword.validatedText(validationType: ValidatorType.password)
            let _ = try self.tfConfirmPassword.validatedText(validationType: ValidatorType.requiredField(field: Strings.CONFIRM_PASSWORD.text))
            if newPassword != confirmPassword{
                throw ValidationError(Strings.PWD_DONT_MATCH.text)
            }
            self.changePassword()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
}
//MARK:- Helper Methods
extension ChangePassword{
    private func changePassword(){
        AppStateManager.sharedInstance.deleteLoggedInUser()
//
//        let old_password = self.tfOldPassword.text ?? ""
//        let new_password = self.tfNewPassword.text ?? ""
//        let params:[String:Any] = ["current_password":old_password,
//                                   "new_password":new_password]
//        APIManager.sharedInstance.usersAPIManager.ChangePassword(params: params, success: { (responseObject) in
//            AppStateManager.sharedInstance.deleteLoggedInUser()
//            Utility.main.showAlert(message: Strings.PASSWORD_CHANGED.text, title: Strings.SUCCESS.text, controller: self, usingCompletionHandler: {
//                AppDelegate.shared.changeRootViewController()
//            })
//        }) { (error) in
//            print(error)
//        }
    }
}

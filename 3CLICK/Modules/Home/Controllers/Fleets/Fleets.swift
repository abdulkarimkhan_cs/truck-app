//
//  Fleets.swift
//  3CLICK
//
//  Created by Sierra-PC on 28/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class Fleets: BaseController {

    @IBOutlet weak var tfSearchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.FLEETS.text
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnFilter(_ sender: Any) {
        self.presentFleetFilter()
    }
}
//MARK:- Helper Methods
extension Fleets{
    private func presentFleetFilter(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FleetsFilter") as! FleetsFilter
        self.present(controller, animated: true, completion: nil)
    }
}
//MARK:- UITableViewDataSource
extension Fleets: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FleetsTVC", for: indexPath) as! FleetsTVC
        return cell
    }
}
//MARK:- UITableViewDelegate
extension Fleets: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.pushToFleetDetails()
    }
}

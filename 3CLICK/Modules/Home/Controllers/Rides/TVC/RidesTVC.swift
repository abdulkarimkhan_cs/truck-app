//
//  RidesTVC.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import HGRippleRadarView

class RidesTVC: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewRipple: RippleView!
    
   func setData(data:Bookings){
    let date = data.deliveryDate ?? "2019-06-19T17:25:59.000Z"
    let formatedDate = Utility.getFormattedDate(date: date, format: "dd MMM yy, EEE h:mm a")
    self.lblDate.text = formatedDate
    
   
    self.lblSource.text = data.pickUpLocation ?? "Pickup"
    self.lblDestination.text = data.dropOffLocation ?? "Dropoff"
    self.lblStatus.text = data.bookingStatus ?? "-"
    self.viewRipple.isHidden = true
    }
}

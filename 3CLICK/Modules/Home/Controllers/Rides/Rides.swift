//
//  Rides.swift
//  3CLICK
//
//  Created by Sierra-PC on 21/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import DZNEmptyDataSet


struct SelectedRideDetailData {
    var bookingType:BookingType
    var ride:Bookings
}

class Rides: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
        
    var bookingType = BookingType.current
    var selectedRide: ((SelectedRideDetailData)->Void)?
    var arrRides = [Bookings]()
    var rideListPaged = PagedModel()
    var pageNumber = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableView()
        // Do any additional setup after loading the view.
        self.addRefreshControl()
        
        
    }
}
//MARK:- Helper Methods
extension Rides{
    private func setTableView(){
        self.tableView.emptyDataSetSource = self
        self.tableView.tableFooterView = UIView()
        self.registerTVC()
    }
    private func registerTVC(){
        self.tableView.register(UINib(nibName: "RidesTVC", bundle: nil), forCellReuseIdentifier: "RidesTVC")
    }
    private func loadMoreCells(){
        let totalRides = self.rideListPaged.count
        if totalRides > self.arrRides.count && self.pageNumber <= self.rideListPaged.totalPages{
            self.pageNumber += 1
            self.getRides()
        }
    }
    private func addRefreshControl(){
        self.refreshControl.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH.text)
        self.refreshControl.addTarget(self, action: #selector(self.refreshRides), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc func refreshRides() {
        // Code to refresh table view
        self.pageNumber = 1
        self.arrRides.removeAll()
        self.tableView.reloadData()
        self.getRides()
    }
}
//MARK:- UITableViewDataSource
extension Rides:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRides.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RidesTVC", for: indexPath) as! RidesTVC
        let data = self.arrRides[indexPath.row]
        cell.setData(data: data)
        //cell.setData(type: self.bookingType)
        return cell
    }
}
//MARK:- UITableViewDelegate
extension Rides:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ride = self.arrRides[indexPath.row]
        let selectedRide = SelectedRideDetailData(bookingType: self.bookingType, ride: ride)
        self.selectedRide?(selectedRide)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if tableView.visibleCells.contains(cell) {
                if indexPath.row == self.arrRides.count - 1{
                    self.loadMoreCells()
                }
            }
        }
    }
}
//MARK:- DZNEmptyDataSetSource
extension Rides:DZNEmptyDataSetSource{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_RIDES.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = Strings.NO_RIDES_DESC.text
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}
//MARK:- Services
extension Rides{
    func getRides(){
        
            let bookingStatus = self.bookingType.rawValue
            let page = self.pageNumber
            let limit = Constants.PAGINATION_PAGE_SIZE
            
            let params:[String:Any] = ["bookingStatus":bookingStatus,
                                       "page":page,
                                       "limit":limit]
            print(params)
            APIManager.sharedInstance.usersAPIManager.RideList(params: params, success: { (responseObject) in
                
                let response = responseObject as Dictionary
                print(response.printJson())
                guard let bookings = response["bookings"] as? [[String:Any]] else {return}
                print(bookings)
                if self.arrRides.isEmpty{
                    self.rideListPaged = Mapper<PagedModel>().map(JSON: responseObject) ?? PagedModel()
                    self.arrRides = Mapper<Bookings>().mapArray(JSONArray: bookings)
                }
                else{
                    let rides = Mapper<Bookings>().mapArray(JSONArray: bookings)
                    self.arrRides += rides
                }
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                
            }) { (error) in
                print(error)
            }
        }
}

//
//  NewPassword.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/21/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit

class NewPassword: BaseController {

    @IBOutlet weak var tfNewPassword: UITextFieldDynamicFonts!
    @IBOutlet weak var tfConfirmPassword: UITextFieldDynamicFonts!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 0:
            self.tfNewPassword.isSecureTextEntry = !self.tfNewPassword.isSecureTextEntry
        case 2:
            self.tfConfirmPassword.isSecureTextEntry = !self.tfConfirmPassword.isSecureTextEntry
        default:
            break
        }
    }
    @IBAction func onBtnChangePassword(_ sender: OrangeGradient) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension NewPassword{
    private func validate(sender:UIButton){
        let newPassword = self.tfNewPassword.text ?? ""
        let confirmPassword = self.tfConfirmPassword.text ?? ""
        do {
            let _ = try self.tfNewPassword.validatedText(validationType: ValidatorType.newPassword)
            if newPassword != confirmPassword{
                throw ValidationError(Strings.PWD_DONT_MATCH.text)
            }
            self.resetPassword()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
}
//MARK:- Service
extension NewPassword{
    private func resetPassword(){
       Utility.main.showAlert(message:Constants.ApiMessage, title: Strings.Confirmation.text, controller: self, usingCompletionHandler: {
                        AppDelegate.shared.changeRootViewController()
                    })
        //          let new_password = self.tfNewPassword.text ?? ""
//        let param:[String:Any] = ["new_password":new_password]
//        APIManager.sharedInstance.usersAPIManager.ResetPassword(params: param, success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message:Constants.ApiMessage, title: Strings.Confirmation.text, controller: self, usingCompletionHandler: {
//                AppDelegate.shared.changeRootViewController()
//            })
//        }) { (error) in
//            print(error)
//        }
    }
}

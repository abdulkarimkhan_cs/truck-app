//
//  EmailVerification.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/21/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import PinCodeTextField

class EmailVerification: BaseController {
    
    @IBOutlet weak var tfEmailPinCode: PinCodeTextField!
    @IBOutlet weak var btnSubmit: OrangeGradient!
    
    var user: UserModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: OrangeGradient) {
        self.validate()
    }
    @IBAction func onBtnResendCode(_ sender: UIButton) {
        self.resendCode()
    }

}
extension EmailVerification{
    private func setUI(){
        self.tfEmailPinCode.keyboardType = .numberPad
    }
    private func validate(){
        let emailPin = self.tfEmailPinCode.text ?? ""
        if emailPin.count != 4{
            Utility.main.showToast(message: Strings.INVALID_EMAIL_CODE.text)
            self.btnSubmit.shake()
            return
        }
        self.verifyCode()
    }
}
//MARK:- Services
extension EmailVerification{
    private func resendCode(){
        Utility.main.showAlert(message: Strings.RESENT_MESSAGE_EMAIL.text, title: Strings.RESENT.text)

//        APIManager.sharedInstance.usersAPIManager.ResendOTP(params: [:], success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message: Strings.RESENT_MESSAGE_EMAIL.text, title: Strings.RESENT.text)
//        }) { (error) in
//            print(error)
//        }
    }
    private func verifyCode(){
       Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.VERIFIED.text, controller: self, usingCompletionHandler: {
                        super.pushToNewPassword()
                    })
//                  let code = self.tfEmailPinCode.text ?? ""
//        let param:[String:Any] = ["code":code]
//        APIManager.sharedInstance.usersAPIManager.VerifyEmailCode(params: param, success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message: Constants.ApiMessage, title: Strings.VERIFIED.text, controller: self, usingCompletionHandler: {
//                super.pushToNewPassword()
//            })
//        }) { (error) in
//            print(error)
//        }
    }
}



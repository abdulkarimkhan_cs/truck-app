//
//  CodeVerification.swift
//  3CLICK
//
//  Created by Shakeel Khan on 10/14/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import PinCodeTextField
import ObjectMapper

class CodeVerification: BaseController {
    
    @IBOutlet weak var tfEmailPinCode: PinCodeTextField!
    @IBOutlet weak var tfPhonePinCode: PinCodeTextField!
    @IBOutlet weak var btnSubmit: OrangeGradient!
    
    var user: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: OrangeGradient) {
        self.validate()
    }
    @IBAction func onBtnResendCode(_ sender: UIButton) {
        self.resendCode()
    }
}
extension CodeVerification{
    private func setUI(){
        self.tfEmailPinCode.keyboardType = .numberPad
        self.tfPhonePinCode.keyboardType = .numberPad
    }
    private func validate(){
        let emailPin = self.tfEmailPinCode.text ?? ""
        let phonePin = self.tfPhonePinCode.text ?? ""
        if emailPin.count != 4{
            Utility.main.showToast(message: Strings.INVALID_EMAIL_CODE.text)
            self.btnSubmit.shake()
            return
        }
        if phonePin.count != 4{
            Utility.main.showToast(message: Strings.INVALID_PHONE_CODE.text)
            self.btnSubmit.shake()
            return
        }
        self.verifyCode()
    }
    private func clearFields(){
        self.tfEmailPinCode.text = ""
        self.tfPhonePinCode.text = ""
    }
}
//MARK:- Services
extension CodeVerification{
    
    private func resendCode(){
        self.clearFields()

        Utility.main.showAlert(message: Strings.RESENT_MESSAGE.text, title: Strings.RESENT.text)
//        APIManager.sharedInstance.usersAPIManager.ResendCode(params: [:], success: { (responseObject) in
//            print(responseObject)
//            Utility.main.showAlert(message: Strings.RESENT_MESSAGE.text, title: Strings.RESENT.text)
//        }) { (error) in
//            print(error)
//      }
    }
    private func verifyCode(){
        AppStateManager.sharedInstance.loginUser(user: UserModel())

//        let emailPin = self.tfEmailPinCode.text ?? ""
//        let phonePin = self.tfPhonePinCode.text ?? ""
//        let code = phonePin + emailPin
//        let param:[String:Any] = ["code":code]
//        APIManager.sharedInstance.usersAPIManager.VerifyOTP(params: param, success: { (responseObject) in
//            print(responseObject)
//            guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
//            print(user)
//            AppStateManager.sharedInstance.loginUser(user: user)
//        }) { (error) in
//            print(error)
//        }
    }
}

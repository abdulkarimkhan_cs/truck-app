//
//  SignUp.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import MobileCoreServices
import PhoneNumberKit
import ObjectMapper
import DropDown

class SignUp: BaseController {

    @IBOutlet weak var tfUserName: UITextFieldDynamicFonts!
    @IBOutlet weak var tfEmailAddress: UITextFieldDynamicFonts!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfCountryCode: UITextFieldDynamicFonts!
    @IBOutlet weak var tfPhoneNumber: UITextFieldDynamicFonts!
    @IBOutlet weak var tfState: UITextFieldDynamicFonts!
    @IBOutlet weak var tfVehicleRegistrationNumber: UITextFieldDynamicFonts!
    @IBOutlet weak var tfPassword: UITextFieldDynamicFonts!
    @IBOutlet weak var tfConfirmPassword: UITextFieldDynamicFonts!
    @IBOutlet weak var tfCertification: UITextField!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    var certificate:NSData?
    var arrFiles = [UploadFileData]()
    var document_url = ""
    let stateDropDown = DropDown()
    var arrStates = ["1","2","3","4"]
    var selectedState: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.getStates()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnCountryCode(_ sender: UIButton) {
        self.openCountryPicker()
    }
    @IBAction func onBtnShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 0:
            self.tfPassword.isSecureTextEntry = !self.tfPassword.isSecureTextEntry
        case 1:
            self.tfConfirmPassword.isSecureTextEntry = !self.tfConfirmPassword.isSecureTextEntry
        default:
            break
        }
    }
    @IBAction func onBtnTermsCheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func onBtnTermsAndConditions(_ sender: UIButton) {
        super.pushToTermsAndConditions()
    }
    @IBAction func onBtnSignUp(_ sender: UIButton) {
        self.validate(sender: sender)
    }

}
//MARK:- Helper Methods
extension SignUp{
    private func validate(sender:UIButton){
        let number = (self.tfCountryCode.text ?? "") + (self.tfPhoneNumber.text ?? "")
        do {
            let _ = try self.tfUserName.validatedText(validationType: ValidatorType.username)
            let _ = try self.tfEmailAddress.validatedText(validationType: ValidatorType.email)
            if !Utility.validatePhoneNumber(number: number){
                sender.shake()
                Utility.main.showToast(message: Strings.INVALID_PHONE.text)
                return
            }
            let _ = try self.tfState.validatedText(validationType: ValidatorType.state)
            let _ = try self.tfVehicleRegistrationNumber.validatedText(validationType: ValidatorType.vehicleReg)
            let _ = try self.tfPassword.validatedText(validationType: ValidatorType.password)
            if (self.tfPassword.text ?? "") != (self.tfConfirmPassword.text ?? ""){
                sender.shake()
                Utility.main.showToast(message: Strings.PWD_DONT_MATCH.text)
                return
            }
            if self.arrFiles.isEmpty{
                sender.shake()
                Utility.main.showToast(message: Strings.ATTACH_CURRENCY_OF_CERTIFICATE.text)
                return
            }
            if !self.btnCheckBox.isSelected{
                sender.shake()
                Utility.main.showToast(message: Strings.PLEASE_AGREE_TO_TNC.text)
                return
            }
            self.uploadFile()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
    private func pushToUploadCertificate(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "UploadCertificate") as! UploadCertificate
        controller.selectedFiles = { selectedFiles in
            self.arrFiles = selectedFiles
        }
        controller.arrFiles = self.arrFiles
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func openCountryPicker(){
        let controller = MICountryPicker()
        controller.delegate = self
        controller.showCallingCodes = true
  //      controller.setStatusBarStyle(.lightContent)
        self.present(controller, animated: true, completion: nil)
    }
    private func setUI(){
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: "AU")?.image()
        self.tfCountryCode.text = "+61"
    }
    private func setStatesDropDown(){
        self.stateDropDown.dataSource = ["LHR","KHI","ISL"]
        self.stateDropDown.direction = .any
        self.stateDropDown.anchorView = self.tfState
        self.stateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfState.text = item
            self.selectedState = self.arrStates[index]
        }
    }
}
//MARK:- Services
extension SignUp: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.tfCertification == textField{
            self.pushToUploadCertificate()
            return false
        }
        if textField == self.tfState{
            self.stateDropDown.show()
            return false
        }
        return true
    }
}
//MARK:- MICountryPickerDelegate
extension SignUp: MICountryPickerDelegate{
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        self.dismiss(animated: true, completion: nil)
        self.imgCountryFlag.image = Utility.emojiFlag(regionCode: code)?.image()
    }
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.tfCountryCode.text = dialCode
        self.tfPhoneNumber.becomeFirstResponder()
    }
}
//MARK:- Services
extension SignUp{
    private func getStates(){
        self.setStatesDropDown()
//        APIManager.sharedInstance.usersAPIManager.GetStates(params: [:], success: { (responseObject) in
//            guard let response = responseObject as? [[String:Any]] else {return}
//            self.arrStates = Mapper<StateModel>().mapArray(JSONArray: response)
//            self.setStatesDropDown()
//        }) { (error) in
//            print(error)
//        }
    }
    private func uploadFile(){
          self.processSignUp()

//        if let file = self.arrFiles.first{
//            let file = file.fileData as Data
//            let param:[String:Any] = ["file":file]
//            print(param)
//            APIManager.sharedInstance.usersAPIManager.UploadFile(params: param, success: { (responseObject) in
//                print(responseObject)
//                let response = responseObject as Dictionary
//                if let doc = response["file_url"] as? String{
//                    self.document_url = doc
//                }
//                self.processSignUp()
//            }) { (error) in
//                print(error)
//            }
    }
    private func processSignUp(){
        
        super.pushToCodeVerification(user: UserModel())
//        let name = self.tfUserName.text ?? ""
//        let email = self.tfEmailAddress.text ?? ""
//        let password = self.tfPassword.text ?? ""
//        let mobile_number = (self.tfCountryCode.text ?? "") + "\(Int(self.tfPhoneNumber.text ?? "") ?? 0)"
//        let state_id = self.selectedState?.stateId ?? "0"
//        let vehicle_reg_no = self.tfVehicleRegistrationNumber.text ?? ""
//        let document_url = self.document_url
//        let user_type = Constants.userType
//        let device_token = Constants.DeviceToken
//        let network_protocol = "APNS"
//        let params:[String:Any] = ["name":name,
//                                   "email":email,
//                                   "password":password,
//                                   "mobile_number":mobile_number,
//                                   "state_id":state_id,
//                                   "vehicle_reg_no":vehicle_reg_no,
//                                   "document_url":document_url,
//                                   "user_type":user_type,
//                                   "device_token":device_token,
//                                   "network_protocol":network_protocol]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.SignUpUser(params: params, success: { (responseObject) in
//            print(responseObject)
//            guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
//            Constants.Token = user.token ?? ""
//            super.pushToCodeVerification()
//        }) { (error) in
//            print(error)
//        }
    }
}

//
//  ForgotPassword.swift
//  3CLICK
//
//  Created by Sierra-PC on 24/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPassword: BaseController {

    @IBOutlet weak var tfEmailAddress: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension ForgotPassword{
    private func validate(sender:UIButton){
        do {
            let _ = try self.tfEmailAddress.validatedText(validationType: ValidatorType.email)
            self.forgotPassword()
        } catch(let error) {
            sender.shake()
            Utility.main.showToast(message: (error as! ValidationError).message)
        }
    }
}

//MARK:- Service
extension ForgotPassword{
    private func forgotPassword(){
        super.pushToEmailVerification()

//        let email = self.tfEmailAddress.text ?? ""
//        let param:[String:Any] = ["email":email]
//        APIManager.sharedInstance.usersAPIManager.ForgotPassword(params: param, success: { (responseObject) in
//            guard let user = Mapper<UserModel>().map(JSON: responseObject) else{return}
//            Constants.Token = user.token ?? ""
//            super.pushToEmailVerification()
//        }) { (error) in
//            print(error)
//        }
    }
}

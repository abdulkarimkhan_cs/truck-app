//
//  CurrentRide.swift
//  3CLICK
//
//  Created by Sierra-PC on 04/09/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CurrentRideModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let rideId = "rideId"
        static let distance = "distance"
    }
    
    // MARK: Properties
    @objc dynamic var id = 0
    @objc dynamic var rideId: String? = ""
    @objc dynamic var distance: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        rideId <- map[SerializationKeys.rideId]
        distance <- map[SerializationKeys.distance]
    }
}

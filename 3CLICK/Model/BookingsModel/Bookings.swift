//
//  Bookings.swift
//
//  Created by Hamza Hasan on 03/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Bookings: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pickUpLocation = "pickUpLocation"
    static let pickUpCity = "pickUpCity"
    static let paymentStatus = "paymentStatus"
    static let deliveryDate = "deliveryDate"
    static let balance = "balance"
    static let commodity = "commodity"
    static let reserveTime = "reserveTime"
    static let returned = "returned"
    static let dropOffLocation = "dropOffLocation"
    static let createdAt = "createdAt"
    static let orderNumber = "orderNumber"
    static let truckType = "truckType"
    static let customer = "customer"
    static let isIntercity = "isIntercity"
    static let paidAmount = "paidAmount"
    static let bookingStatus = "bookingStatus"
    static let isDeleted = "isDeleted"
    static let id = "_id"
    static let deliveryAddress = "deliveryAddress"
    static let v = "__v"
    static let updatedAt = "updatedAt"
    static let weight = "weight"
    static let createdBy = "createdBy"
    static let pickUpAddress = "pickUpAddress"
    static let amount = "amount"
  }

  // MARK: Properties
  @objc dynamic var pickUpLocation: String? = ""
  @objc dynamic var pickUpCity: PickUpCity?
  @objc dynamic var paymentStatus: String? = ""
  @objc dynamic var deliveryDate: String? = ""
  @objc dynamic var balance = 0
  @objc dynamic var commodity: String? = ""
  @objc dynamic var reserveTime = 0
  @objc dynamic var returned = 0
  @objc dynamic var dropOffLocation: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var orderNumber: String? = ""
  @objc dynamic var truckType: String? = ""
  @objc dynamic var customer: Customer?
  @objc dynamic var isIntercity = false
  @objc dynamic var paidAmount = 0
  @objc dynamic var bookingStatus: String? = ""
  @objc dynamic var isDeleted = false
  @objc dynamic var id: String? = ""
  @objc dynamic var v = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var weight: String? = ""
  @objc dynamic var createdBy: String? = ""
  @objc dynamic var amount = 0


  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    pickUpLocation <- map[SerializationKeys.pickUpLocation]
    pickUpCity <- map[SerializationKeys.pickUpCity]
    paymentStatus <- map[SerializationKeys.paymentStatus]
    deliveryDate <- map[SerializationKeys.deliveryDate]
    balance <- map[SerializationKeys.balance]
    commodity <- map[SerializationKeys.commodity]
    reserveTime <- map[SerializationKeys.reserveTime]
    returned <- map[SerializationKeys.returned]
    dropOffLocation <- map[SerializationKeys.dropOffLocation]
    createdAt <- map[SerializationKeys.createdAt]
    orderNumber <- map[SerializationKeys.orderNumber]
    truckType <- map[SerializationKeys.truckType]
    customer <- map[SerializationKeys.customer]
    isIntercity <- map[SerializationKeys.isIntercity]
    paidAmount <- map[SerializationKeys.paidAmount]
    bookingStatus <- map[SerializationKeys.bookingStatus]
    isDeleted <- map[SerializationKeys.isDeleted]
    id <- map[SerializationKeys.id]
    v <- map[SerializationKeys.v]
    updatedAt <- map[SerializationKeys.updatedAt]
    weight <- map[SerializationKeys.weight]
    createdBy <- map[SerializationKeys.createdBy]
    amount <- map[SerializationKeys.amount]
  }


}

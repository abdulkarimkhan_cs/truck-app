//
//  BookingsModel.swift
//
//  Created by Hamza Hasan on 03/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class BookingsModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalPages = "totalPages"
    static let bookings = "bookings"
    static let page = "page"
    static let count = "count"
  }

  // MARK: Properties
  @objc dynamic var totalPages = 0
  var bookings = List<Bookings>()
  @objc dynamic var page = 0
  @objc dynamic var count = 0
  @objc dynamic var id = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalPages <- map[SerializationKeys.totalPages]
    bookings <- (map[SerializationKeys.bookings], ListTransform<Bookings>())
    page <- map[SerializationKeys.page]
    count <- map[SerializationKeys.count]
  }


}

//
//  Vendor.swift
//
//  Created by Hamza Hasan on 04/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Vendor: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "_id"
    static let image = "image"
    static let email = "email"
    static let contact = "contact"
    static let firstName = "firstName"
    static let lastName = "lastName"
  }

  // MARK: Properties
  @objc dynamic var id: String? = ""
    @objc dynamic var image: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var contact: String? = ""
    @objc dynamic var firstName: String? = ""
    @objc dynamic var lastName: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    image <- map[SerializationKeys.image]
    email <- map[SerializationKeys.email]
    contact <- map[SerializationKeys.contact]
    firstName <- map[SerializationKeys.firstName]
    lastName <- map[SerializationKeys.lastName]
  }


}

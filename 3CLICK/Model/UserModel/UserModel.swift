//
//  UserModel.swift
//
//  Created by Hamza Hasan on 23/12/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class UserModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let id = "_id"
    static let profilePic = "profilePic"
    static let email = "email"
    static let accessToken = "accessToken"
    static let contact = "contact"
    static let lastName = "lastName"
    static let deviceId = "deviceId"
    static let firstName = "firstName"
    static let role = "role"
  }

  // MARK: Properties
  @objc dynamic var status: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var profilePic: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var accessToken: String? = ""
  @objc dynamic var contact: String? = ""
  @objc dynamic var lastName: String? = ""
  @objc dynamic var deviceId: String? = ""
  @objc dynamic var firstName: String? = ""
  @objc dynamic var role: Role?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    profilePic <- map[SerializationKeys.profilePic]
    email <- map[SerializationKeys.email]
    accessToken <- map[SerializationKeys.accessToken]
    contact <- map[SerializationKeys.contact]
    lastName <- map[SerializationKeys.lastName]
    deviceId <- map[SerializationKeys.deviceId]
    firstName <- map[SerializationKeys.firstName]
    role <- map[SerializationKeys.role]
  }


}

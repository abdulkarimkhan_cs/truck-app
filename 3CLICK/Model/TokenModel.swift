//
//  BaseClass.swift
//
//  Created by Shakeel Khan on 6/25/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class TokenModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kBaseClassTokenKey: String = "token"

  // MARK: Properties
  public var token: String?

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
    required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    token <- map[kBaseClassTokenKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = token { dictionary[kBaseClassTokenKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.token = aDecoder.decodeObject(forKey: kBaseClassTokenKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(token, forKey: kBaseClassTokenKey)
  }

}

//
//  RideListModel.swift
//  3CLICK
//
//  Created by Shakeel Khan on 6/25/19.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class RideModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let pickup_location_name = "pickup_location"
        static let drop_off_location_name = "dropoff_location"
        static let booking_date = "booking_date"
        static let expire_date_time = "expire_date_time"
        static let ride_id = "ride_id"
        static let amount = "amount"
        static let rideStatusId = "ride_status_id"
        static let rideStatusName = "ride_status_name"
    }
    // MARK: Properties
    @objc dynamic var pickup_location_name: String? = ""
    @objc dynamic var drop_off_location_name: String? = ""
    @objc dynamic var booking_date: String? = ""
    @objc dynamic var expire_date_time: String? = ""
    @objc dynamic var ride_id = 0
    @objc dynamic var amount: String? = ""
    @objc dynamic var rideStatusId = 0
    @objc dynamic var rideStatusName: String? = ""
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "ride_id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        pickup_location_name <- map[SerializationKeys.pickup_location_name]
        drop_off_location_name <- map[SerializationKeys.drop_off_location_name]
        booking_date <- map[SerializationKeys.booking_date]
        expire_date_time <- map[SerializationKeys.expire_date_time]
        ride_id <- map[SerializationKeys.ride_id]
        amount <- map[SerializationKeys.amount]
        rideStatusId <- map[SerializationKeys.rideStatusId]
        rideStatusName <- map[SerializationKeys.rideStatusName]
    }
}

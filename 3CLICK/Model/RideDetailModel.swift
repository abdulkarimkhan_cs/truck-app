//
//  BookingDetailModel.swift
//
//  Created by Hamza Hasan on 04/01/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class RideDetailModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let deliveryDate = "deliveryDate"
    static let pickUpLatitude = "pickUpLatitude"
    static let id = "_id"
    static let bookingStatus = "bookingStatus"
    static let pickUpLocation = "pickUpLocation"
    static let dropOffLocation = "dropOffLocation"
    static let createdAt = "createdAt"
    static let dropOffLatitude = "dropOffLatitude"
    static let weight = "weight"
    static let amount = "amount"
    static let pickUpLongitude = "pickUpLongitude"
    static let dropOffLongitude = "dropOffLongitude"
    static let vendor = "vendor"
    static let customer = "customer"
  }

  // MARK: Properties
  @objc dynamic var deliveryDate: String? = ""
  @objc dynamic var pickUpLatitude: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var bookingStatus: String? = ""
  @objc dynamic var pickUpLocation: String? = ""
  @objc dynamic var dropOffLocation: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var dropOffLatitude: String? = ""
  @objc dynamic var weight: String? = ""
  @objc dynamic var amount = 0
  @objc dynamic var pickUpLongitude: String? = ""
  @objc dynamic var dropOffLongitude: String? = ""
  @objc dynamic var vendor: Vendor?
  @objc dynamic var customer: Vendor?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

  override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    deliveryDate <- map[SerializationKeys.deliveryDate]
    pickUpLatitude <- map[SerializationKeys.pickUpLatitude]
    id <- map[SerializationKeys.id]
    bookingStatus <- map[SerializationKeys.bookingStatus]
    pickUpLocation <- map[SerializationKeys.pickUpLocation]
    dropOffLocation <- map[SerializationKeys.dropOffLocation]
    createdAt <- map[SerializationKeys.createdAt]
    dropOffLatitude <- map[SerializationKeys.dropOffLatitude]
    weight <- map[SerializationKeys.weight]
    amount <- map[SerializationKeys.amount]
    pickUpLongitude <- map[SerializationKeys.pickUpLongitude]
    dropOffLongitude <- map[SerializationKeys.dropOffLongitude]
    vendor <- map[SerializationKeys.vendor]
    customer <- map[SerializationKeys.customer]
  }


}

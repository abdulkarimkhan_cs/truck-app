
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class PagedModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let page = "page"
        static let totalPages = "totalPages"
        static let count = "count"
        static let id = "id"
    }
    
    // MARK: Properties
    @objc dynamic var page = 0
    @objc dynamic var totalPages = 0
    @objc dynamic var id = 0
    @objc dynamic var count = 0
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        page <- map[SerializationKeys.page]
        totalPages <- map[SerializationKeys.totalPages]
        count <- map[SerializationKeys.count]
    }
}

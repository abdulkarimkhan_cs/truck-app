//
//  EarningModel.swift
//  3CLICK
//
//  Created by Sierra-PC on 30/08/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class EarningModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id              = "id"
        static let averageRating          = "average_rating"
        static let cancelledRides            = "cancelled_rides"
        static let completedRides        = "completed_rides"
        static let rejectedRides          = "average_rating"
        static let totalEarning            = "cancelled_rides"
        static let totalRides        = "completed_rides"
        
        
    }
    // MARK: Properties
    @objc dynamic var id                : String? = ""
    @objc dynamic var averageRating     : String? = ""
    @objc dynamic var cancelledRides    : String? = ""
    @objc dynamic var completedRides    : String? = ""
    @objc dynamic var rejectedRides     : String? = ""
    @objc dynamic var totalEarning      : String? = ""
    @objc dynamic var totalRides        : String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id                 <- map[SerializationKeys.id      ]
        averageRating             <- map[SerializationKeys.averageRating  ]
        cancelledRides               <- map[SerializationKeys.cancelledRides    ]
        completedRides           <- map[SerializationKeys.completedRides]
        rejectedRides             <- map[SerializationKeys.rejectedRides  ]
        totalEarning               <- map[SerializationKeys.totalEarning    ]
        totalRides           <- map[SerializationKeys.totalRides]
        
    }
}

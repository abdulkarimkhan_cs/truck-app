
import Foundation
import UIKit
import LGSideMenuController

class BaseController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNotificationObserver()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigation()
    }
}
//MARK:- Add Navigation Items
extension BaseController{
    private func addBackBarButtonItem() {
        let image = UIImage(named: "back")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    private func addSideMenuBarButtonItem() {
        let image = UIImage(named: "menuWhite")
        let sideMenuItem = UIBarButtonItem(image: image,
                                           style: .plain,
                                           target: self,
                                           action: #selector(self.onBtnSideMenu))
        
        self.navigationItem.leftBarButtonItem = sideMenuItem
    }
    private func addTruckLogo() {
        let image = UIImage(named: "truckLogo")
        let truckLogo = UIBarButtonItem(image: image,
                                           style: .plain,
                                           target: self,
                                           action: nil)
        self.navigationItem.rightBarButtonItem = truckLogo
    }
}
//MARK:- Selectors
extension BaseController{
    @objc func onBtnSideMenu(){
        sideMenuController?.showLeftViewAnimated()
    }
    @objc func onBtnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func notificationObserver(notification: NSNotification) {
        self.pushToEditProfile()
    }
}
//MARK:- Navigation Methods
extension BaseController{
    func pushToHome(rideStatus:RideStatus,rideDetail:RideDetailModel){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
        controller.rideDetail = rideDetail
        controller.rideStatus = rideStatus
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToEditProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EditProfile")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToSignUp(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUp")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToLogin(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Login")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToForgotPassword(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ForgotPassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToFleets(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Fleets")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToFleetDetails(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FleetDetails")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToProfile(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Profile")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToBankingDetails(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "BankingDetails")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToChangePassword(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "ChangePassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToRideDetails(rideDetail:RideDetailModel,rideType:BookingType, flagShouldChangeToRootView: Bool){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "RideDetail") as! RideDetail
        controller.flagShouldChangeToRootView = flagShouldChangeToRootView
        controller.rideType = rideType
        controller.rideDetail = rideDetail
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func pushToRideDetails(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "RideDetail") 
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToCustomerRating(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomerRating")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToCodeVerification(user:UserModel){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CodeVerification") as! CodeVerification
        controller.user = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToEmailVerification(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EmailVerification")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToNewPassword(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "NewPassword")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func pushToTermsAndConditions(){
        let storyboard = AppStoryboard.Login.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as! CMS
        controller.cmsType = .termsAndConditions
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Helper Methods
extension BaseController{
    private func addNotificationObserver(){
        let notificationName = Notification.Name("PushToEditProfile")
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationObserver), name: notificationName, object: nil)
    }
    private func resetNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    private func setNavigation(){
        let currentClassName = Utility.main.topViewController()?.className
        guard let navControllerCount = self.navigationController?.viewControllers.count else {return}
        if navControllerCount > 1 && AppStateManager.sharedInstance.isUserLoggedIn(){
            self.sideMenuController?.isLeftViewSwipeGestureEnabled = false
            self.navigationController?.navigationBar.isHidden = false
            self.addBackBarButtonItem()
        }
        else{
            self.navigationController?.navigationBar.isHidden = true
        }
        if currentClassName == "LGSideMenuController" && navControllerCount == 1{
            self.sideMenuController?.isLeftViewSwipeGestureEnabled = true
            self.navigationController?.navigationBar.isHidden = false
            self.addSideMenuBarButtonItem()
        }
//        if currentClassName == "UploadCertificate"{
//            self.navigationController?.navigationBar.isHidden = false
//        }
        self.resetNavigationBar()
    }
    func alignTitleToLeft(){
        let titleLabel = UILabel()
        titleLabel.text = self.navigationItem.title
        titleLabel.textColor = .white
        titleLabel.sizeToFit()
        let leftItem = UIBarButtonItem(customView: titleLabel)
        if self.navigationItem.leftBarButtonItems?.count ?? 0 == 1{
            self.navigationItem.leftBarButtonItems?.append(leftItem)
        }
        else{
            self.navigationItem.leftBarButtonItems?.remove(at: 1)
            self.navigationItem.leftBarButtonItems?.append(leftItem)
        }
        self.navigationItem.title = nil
    }
}

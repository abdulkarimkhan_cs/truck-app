
import Foundation
import UIKit
import AVFoundation
import Toast_Swift
import NVActivityIndicatorView
import PhoneNumberKit

//// MARK: AppHelperUtility setup
@objc class Utility: NSObject{
    static let main = Utility()
    fileprivate override init() {}
}
extension Utility{
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    func openURL(url:URL){
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
        else{
            self.showAlert(message: Strings.URL_NOT_VALID.text ,title: Strings.ERROR.text)
        }
    }
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}
//MARK:- INDICATOR
extension Utility{
    static func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballBeat, color: Global.APP_COLOR, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    static func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
//MARK:- Make phone call
extension Utility{
    func makeCallTo(number:String){
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number)
    }
}
// MARK: Alert related functions
extension Utility{
    func showAlert(message:String,title:String,controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.view.tintColor = Global.APP_COLOR
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil){
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            return alertController
        }
        else{
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            return nil
        }
    }
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void)){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    func showAlert(message:String,title:String,YES:String,NO:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: YES, style: UIAlertAction.Style.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: NO, style: UIAlertAction.Style.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: Strings.YES.text, style: UIAlertAction.Style.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: Strings.NO.text, style: UIAlertAction.Style.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            Utility.main.topViewController()?.present(alertController, animated: true, completion: nil)
            
        }
    }
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    func showActivityViewController(with sharingText: String){
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
    func showToast(message:String){
        Utility.main.showAlert(message: message, title: Strings.ERROR.text)
//        Utility().topViewController()?.view.makeToast(message, duration: 2.0, position: .center, title: nil, image: nil, style: .init(), completion: nil)
    }
}
//MARK:- Utility Methods
extension Utility{
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
//        dateFormatter.timeZone = TimeZone.current
//        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
//            dateFormatter.locale = Locale(identifier: "en_US")
//        }
//        else{
//            dateFormatter.locale = Locale(identifier: "ar_SA")
//        }
        return dateFormatter.string(from: date ?? Date())
    }
    static func getCurrentDate() -> String {
        let dateFormat = "yyyy-MM-dd"
        let toDate = Date()
        let fromDate = Calendar.current.date(byAdding: .month, value: 0, to: toDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: fromDate ?? Date())
    }
    static func getOneMonthEarlier() -> String {
        let dateFormat = "yyyy-MM-dd"
        let toDate = Date()
        let fromDate = Calendar.current.date(byAdding: .month, value: -1, to: toDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: fromDate ?? Date())
    }
    static func showLanguageAlert(language:String, message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.view.tintColor = .black
        if language == "en"{
            alertController.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default){ (alertActionYES) in
                completionHandler(alertActionYES, nil)
            })
            alertController.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel){ (alertActionNO) in
                completionHandler(nil, alertActionNO)
            })
        }
        else{
            alertController.addAction(UIAlertAction(title: "نعم فعلا", style: UIAlertAction.Style.default){ (alertActionYES) in
                completionHandler(alertActionYES, nil)
            })
            alertController.addAction(UIAlertAction(title: "لا", style: UIAlertAction.Style.cancel){ (alertActionNO) in
                completionHandler(nil, alertActionNO)
            })
        }
        controller.present(alertController, animated: true, completion: nil)
    }
    static func getAgeFromDate(dateStr:String)->String{
        let dateFormat = "dd/MM/yyyy"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let birthday = dateFormatter.date(from: dateStr) ?? Date()
        
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year ?? 0
        return "\(age)"
    }
    static func getAllCountries()->[String]{
        var arrCountries = [String]()
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            arrCountries.append(name)
        }
        return arrCountries
    }
    static func emojiFlag(regionCode: String) -> String? {
        let code = regionCode.uppercased()
        
        guard Locale.isoRegionCodes.contains(code) else {
            return nil
        }
        
        var flagString = ""
        for s in code.unicodeScalars {
            guard let scalar = UnicodeScalar(127397 + s.value) else {
                continue
            }
            flagString.append(String(scalar))
        }
        return flagString
    }
    static func validatePhoneNumber(number:String)->Bool{
        let phoneNumberKit = PhoneNumberKit()
        do {
            let _ = try phoneNumberKit.parse(number).countryCode
            return true
        }
        catch {
            return false
        }
    }
}
//MARK:- QueryURL
extension Utility{
    static func URLforRoute(route: String,params:[String: Any]) -> NSURL? {
        if let components: NSURLComponents  = NSURLComponents(string: route){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            return components.url as NSURL?
        }
        return nil
    }
}

extension Utility{
    static func getFormattedDate(date:String?,format:String)->String{
        let dateString = date ?? "2019-06-25T03:37:29.000Z"
        let formattedDate = Utility.stringDateFormatter(dateStr: dateString, dateFormat: Constants.serverDateFormat, formatteddate: format)
        return formattedDate
    }
}

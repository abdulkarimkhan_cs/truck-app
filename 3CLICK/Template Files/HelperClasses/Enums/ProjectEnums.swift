//
//  ProjectEnums.swift
//  3CLICK
//
//  Created by Sierra-PC on 29/06/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import Foundation

enum UserSubType:String{
    case individualRider = "0"
    case companyRider = "1"
}

enum RideStatus{
    case noRide
    case acceptedRide
    case arrived
    case endRide
    case rideEnded
}

enum BookingStatus:String{
    case accepted = "accepted"
    case inprogress = "inprogress"
    case none = "none"
}

enum BookingType:String{
    case current = "current"
    case finished = "finished"
}
//Ride Status Id
//1  for Pending
//    2  for Approved
//3  for Rejected
//    4  for In Progress
//5  for Completed
//    6  for Cancelled

enum CurrentRideStatus:Int{
    case pending    = 1
    case approved   = 2
    case rejected   = 3
    case inProgress = 4
    case completed  = 5
    case cancel     = 6
}

enum CMSType{
    case help
    case aboutUs
    case termsAndConditions
}

enum RideStatusEnum{
    case accepted
    case rejected
}

struct RideStatusStruct {
    var ride_id: Int
    var ride_status:RideStatusEnum
}

//
//  AppDelegate.swift
//  3CLICK
//
//  Created by Sierra-PC on 17/05/2019.
//  Copyright © 2019 Sierra-PC. All rights reserved.
//

import UIKit
import LGSideMenuController
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
import ObjectMapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    let notificationCenter = UNUserNotificationCenter.current()
    var cms:CMSModel?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application
        IQKeyboardManager.shared.enable = true
        self.changeRootViewController()
        GMSServices.provideAPIKey(Constants.apiKey)
        GMSPlacesClient.provideAPIKey(Constants.apiKey)
        self.setupPushNotifications(application: application)
        self.getCMS()
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
extension AppDelegate{
    private func showWelcome(){
        let storyboard = AppStoryboard.Login.instance
        let navigationController = BaseNavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Welcome"))
        navigationController.navigationBar.isHidden = true
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.window?.rootViewController = navigationController
        }, completion: nil)
    }
    private func showLogin(){
        let storyboard = AppStoryboard.Login.instance
        let navigationController = BaseNavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Login"))
        navigationController.navigationBar.isHidden = true
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.window?.rootViewController = navigationController
        }, completion: nil)
    }
    private func showYourRides(){
        let storyboard = AppStoryboard.Home.instance
        let leftMenuController = storyboard.instantiateViewController(withIdentifier: "SideMenu")
        let controller = storyboard.instantiateViewController(withIdentifier: "YourRides")
        let navController = BaseNavigationController(rootViewController: controller)
        let sideMenuController = LGSideMenuController(rootViewController: navController,
                                                      leftViewController: leftMenuController,
                                                      rightViewController: nil)
        if let window = self.window {
            window.rootViewController = nil
            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                sideMenuController.leftViewWidth = window.frame.width * 0.80
                window.rootViewController = sideMenuController
            }, completion: nil)
        }
    }
    func changeRootViewController(){
        if !AppStateManager.sharedInstance.isUserLoggedIn(){
            self.showLogin()
        }
        else{
            self.showYourRides()
        }
    }
}
//MARK:- Push notifications
extension AppDelegate{
    private func setupPushNotifications(application: UIApplication){
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        self.notificationCenter.delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        application.registerForRemoteNotifications()
    }
}
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Constants.DeviceToken = fcmToken
        print(Constants.DeviceToken)
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        completionHandler()
    }
    
}
//MARK:- Services
extension AppDelegate{
    private func getCMS(){
//        APIManager.sharedInstance.usersAPIManager.CMS(params: [:], success: { (responseObject) in
//            guard let cms = Mapper<CMSModel>().map(JSON: responseObject) else{return}
//            self.cms = cms
//        }) { (error) in
//            print(error)
//        }
    }
}
